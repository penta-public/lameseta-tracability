"use strict";
require('dotenv').config(); 

const bip39                           = require('bip39');
const base58                          = require('bs58');
const keys                            = require('./GenerateKeyPair.js');
const { from, timer }                 = require('rxjs');
const { map, filter, flatMap, share } = require('rxjs/operators');
const { of }                          = require('rxjs');
const fs                              = require('fs');
const queryString                     = require('qs');
const _                               = require('lodash');
const crypto                          = require('crypto');
const salt                            = require('../utilities/Salt.js');
const aes                             = require('../utilities/AES_GCM_256_Encryption.js');
const winston                         = require('../winston/winston.js')(__filename);
const driver                          = require('bigchaindb-driver');
const Orm                             = require('bigchaindb-orm')

class BlockExplorer {

    constructor(){
        //this.connection = new driver.Connection('https://test.bigchaindb.com/api/v1/');
        this.connection   = new driver.Connection('http://localhost:9984/api/v1/')
        //this.url        = 'https://test.bigchaindb.com/api/v1/';
        //this.connection = new driver.Connection( this.url );
        // this.seed       = bip39.mnemonicToSeed('Four Score and Twenty Years Ago...').slice(0,32);
        // this.pentaKey   = new driver.Ed25519Keypair(this.seed);
        this.pentaKey     = keys.Ed25519Keypair();
        this.salt         = new salt.Salt('sha512', 64);
        this.cipher       = new aes.aes_256_gcm();
        this.top_of_chain = 0
    };

    async findBlockHeight(){
        index    = 1;
        finished = false;

        while ( ! finished){
            try {
                await this.connection.getBlock( index )
                      .then(  (tx)  => { index +=1 })
                      .catch( (err) => { console.log( err ) });
            }
            catch(err) {
                console.log( block_height );
                finished = true;
                this.top_of_chain = block_height;
            };
        }
        return block_height;
    };
}

module.exports.BlockExplorer = BlockExplorer;
