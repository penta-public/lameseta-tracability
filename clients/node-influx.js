require('dotenv').config(); //GET THE ENVIRONMENT
const _ = require('lodash'); //goodies
const moment = require('moment'); //for timestamp handling
const fetch = require('node-fetch'); //post data
const Influx = require('influx');
const winston  = require('../winston/winston')(__filename);

const TIMEOUT = 10000; //How long to wait inbetween pings
/*************************************************
 * starting up influx
 * influxd -config /usr/local/etc/influxdb.conf
 */
class NodeInflux {
    constructor(config, Schema) {
        /* config.schema = [{
              measurement: 'response_times',
              fields: {
                path: Influx.FieldType.STRING,
                duration: Influx.FieldType.INTEGER
              },
              tags: [ 'host' ]
            }] 
        */
        config.schema = [{ //using static getters
            measurement: Schema.measurement,
            fields: Schema.fields,
            tags: Schema.tags,
        }];
        this._influx = new Influx.InfluxDB(config);
        this.config = config;
    } //eo constructor

    async ping() {
        winston.info(`Pinging Influx with timeout ${TIMEOUT}`);
        let online = false;
        const success = (hosts) => {
            hosts.map(host => {
                host.online ? winston.info(`${host.url.host} responded in ${host.rtt}ms running ${host.version})`) : winston.info(`${host.url.host} is offline :(`);  
                online = online || host.online;
            });
            return online;
        };
        const failure = () => {  
            return online = false;   
        }
        try {
            return this._influx.ping(TIMEOUT).then(success,failure);
        } catch(err) {
            winston.error(err);
            return false;
        }
    } //eo ping

    async init() {
        return this._influx.getDatabaseNames().then(names => {
          if (!names.includes(this.config.database)) {
            winston.info(`Creating Influx database ${this.config.database}`);
            return this._influx.createDatabase(this.config.database);
          } else {
            return Promise.resolve(this.config.database);
          }
        });
    } //eo init

    async readPoints(measurement) {
        const query = () => {
            return this._influx.query(`
              select * from "${measurement}"
              order by time desc
            `)
        };
        const list = (rows) => {
            rows.forEach(row => console.log(`A request to ${measurement} temp: ${row.Temperature_C} at ${row.Logged}`))
            return rows;
        };
        const failure = (err) => {
            console.log(err)
        };
        return measurement ? query().then(list, failure) : Promise.reject(`No measurement provided to Influx.readPoints`);
    }

    async writePoints(d) {
        try {
            const options = {precision:'s'}; //options when writing to influx
            const measurement = d.length > 0 ? d[0].measurement : null;
            const query = () => {
                return this._influx.query(`
                  select * from "${measurement}"
                  order by time desc
                `)
            };
            const list = (rows) => {
                winston.info("Listing rows")
                // rows.forEach(row => console.log(` ${measurement} temp: ${row.Temperature_C} at ${row.Logged} / time ${row.time} hash: ${row.BigChain_Hash}`))
            };
            const failure = (err) => {
                console.log(err)
            };
            if(!measurement) { return winston.error(`Influx.writePoints data length = 0`)};
            await this.init();
            winston.info("Points to be written " + JSON.stringify(d));
            this._influx.writePoints(d, options).then(query, failure)
                .then(list, failure)
                .catch( (err) => Promise.reject(err));
        } catch(err) {
            return Promise.reject(err); //throw triggers a retry
        }
    } //eo writePoints
} //eo NodeInflux

module.exports = NodeInflux;