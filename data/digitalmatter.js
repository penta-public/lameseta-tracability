const moment = require('moment');
const _      = require('lodash');
const Schema    = require('./schema.0.0.4'); //0.0.3 combines RB and DM, plus str -> fieldtype conversion

//-----------------------------------------------------
//
//  Files to convert 
const logReason = require('../data/Log-Reasons.json');
const dmFields  = require('../data/DM_Field_Structure.json');
const dmTags    = require('../data/DM_Tag_Structure.json');
const dmF21Data = require('../data/DM_F21_Data.json');

class DigitalMatter { 
    constructor(measurement) {
        this.measurement  = measurement;
        this._point       = null;
        this._pentaTags   = dmTags;
        this._pentaFields = dmFields;
        this._dmF21Data   = dmF21Data;
    };

    /* 
        d: Object 
        point: Object
        measurement	- String = Measurement is the Influx measurement name.
        tags - Object<String, String>	optional = Tags is the list of tag values to insert.
        fields - Object<String, *>	optional = Fields is the list of field values to insert.
        timestamp - Date | string | number	optional = Specifies a timestamp for this point. 
        This can be a Date object, in which case we'll adjust it to the desired precision, 
        or a numeric string or number, in which case it gets passed directly to Influx.
    */   
    set point(__point) { this._point = __point }
    get point() { return this._point; }


    preprocess(d, _d) {
        _.assign(d, _d); //use lodash implementation
        return d;
    }

    makePoint(d) {
        /*
        //this bit of magic from: https://medium.freecodecamp.org/30-seconds-of-code-rename-many-object-keys-in-javascript-268f279c7bfa
        //see pdf in notes/  usage:
        keysMap = { name: 'firstName', job: 'passion' };
        obj = { name: 'Bobo', job: 'Front-End Master' };
        renameKeys(keysMap, obj);
        // { firstName: 'Bobo', passion: 'Front-End Master' } 
        */

        const renameKeys = (keysMap, obj) => Object.keys(obj).reduce((acc, key) => ({
            ... acc,
            ... keysMap[key] ? { [keysMap[key] || key]: obj[key] } : {}
        }), {});
        const convertFieldTypes = (o) => {
            Object.keys(o).map(key => {
                if(o[key] === undefined || o[key] === null) {
                    delete o[key];
                    return;
                }
                const fieldType = Schema.fields[key];
                o[key] = Schema.convertType(fieldType, o[key]);
            });
            return o;
        };
        const prepareTimestamp = (_d) => {
            //   { '﻿Date Logged (AET (+10:00))': '27/03/2019 20:52',
            let ts = null;
            let logged = null;
            _.map(Object.keys(_d), (key) => {
                if(!key.includes('Logged')) { return; }
                logged = _d[key];
                ts = _d[key];
                ts = moment(ts, 'DD/MM/YYYY HH:mm').toDate();
            })
            return {ts:ts, logged:logged};
        }; //End of prepareTime

        const tags = renameKeys( this._pentaTags, d);
        let fields = renameKeys(this._pentaFields, d);
        fields = convertFieldTypes(fields);
        const point = {
            measurement: this.measurement,
            tags: tags,
            fields: fields
       };
       const timestamp = prepareTimestamp(d);
       if(_.isDate(timestamp.ts)) {
            point.timestamp = timestamp.ts;
            point.fields.Logged = timestamp.logged;
       }
       return point;
    }

    pointFromData(d) {
        // d.Records is a list of fields. Eacj field is formatted differently and has a 
        // a different format according to the documents 'Digital Matter Telematics Device 
        // Integration over HTTP, V1.6 12 October 2018. The indiviudal parsing functions below 
        // extract the components required to build the standardised object used here. 
        // Each Field type is identified by its Ftype field which is in the range [0, 21].
        // Nothing pretty here - I wish there was - just a straight forward digging the 
        // of the data out the DM structure and inserting into ours.
        //
        // F0 - GPS Data
        const F0 = (F, pField) => {
                    pField.Latitude            = F.Lat;
                    pField.Longitude           = F.Long;
                    pField.Altitude_m          = F.Alt;
                    pField.Speed_KmH           = F.Spd;
                    pField.Speed_Accuracy_KmH  = F.SpdAcc;
                    pField.Heading_Degrees     = F.Heading;
                    pField.Position_Accuracy_m = F.PosAcc;
                    pField.PDOP_Raw            = F.PDOP;
                    let val                    = F.GpsStat;
                    val         = (typeof val === 'number' || typeof val === 'string') && !isNaN(Number(val)) ? _.toNumber(val) : val;
                    let GpsStat = val.toString(2);
                    pField.Gps_Status_Fix_OK = GpsStat[0];
                    pField.Gps_Status_Fix_3D = GpsStat[1];
                    return pField;
        };
        // F2 - Device Status Data
        const F2 = (F, pField) => {
                    let val         = F.DevStat; 
                    val             = (typeof val === 'number' || typeof val === 'string') && !isNaN(Number(val)) ? _.toNumber(val) : val;
                    let DevStat     = val ? val.toString(2) : "0";
                    pField.Tamper   = DevStat[6];
                    return pField;
        }; 
        // F6 - Analogue Battery Data
        const F6 = (F, pField) => {
                    pField.Battery_Voltage_V    = F["1"];
                    pField.Temperature_Sensor_C = F["3"];
                    return pField;
        };
        // F7 - Analogue Data
        const F7 = (F, pField) => {
            if (F.AnalogueData) {
                pField.Temperature_C             = F.AnalogueData["5"]  ? F.AnalogueData["5"] : null;
                pField.Relative_Humidity_Percent = F.AnalogueData["6"]  ? F.AnalogueData["6"] : null;
                pField.Luminosity                = F.AnalogueData["11"] ? F.AnalogueData["11"] : null;
            };
            return pField;   
        };
        // F15 - Trip Type Data  
        const F15 = (F, pField) => {
                    let val               = F.TT; 
                    val                   = (typeof val === 'number' || typeof val === 'string') && !isNaN(Number(val)) ? _.toNumber(val) : val;
                    pField.Trip_Type_Code = val ? val : 0;
                    return pField;
        };
        const mkPoint = (point) => {
            let tags      = this._pentaTags;
            let fields    = this._pentaFields;
            tags.Company  = "Penta USA";
            tags.Project  = "Michigan Apples Project";
            tags.SensorID = d.SerNo;
            point.tags    = tags;
            return point;
        };
        const extractFields = ( dmFields ) => ( dmRecord ) => {
            let pFields        = dmFields;
            pFields.Status     = logReason[ dmRecord.Reason.toString() ];
            pFields.Time_Stamp = dmRecord.DateUTC;

            dmRecord.Fields.forEach( (fld ) => {
                switch ( fld.FType ) {
                    case 0  : pFields = F0(fld, pFields);
                    case 2  : pFields = F2(fld, pFields);
                    case 6  : pFields = F6(fld, pFields);
                    case 7  : pFields = F7(fld, pFields);
                    case 15 : pFields = F15(fld, pFields);
                    default : 
                };
            });
            return pFields;
        };
        // First create some Penta Fields. 
        const fields   = this._pentaFields; 

        // Get the records in the reading. 
        let recordList = d.Records.map( extractFields(fields) );

        // Create point. 

        let pointList = recordList.map( mkPoint );''
        return pointList;
    } //End of pointFromData

    pointFromCSV(d) {
        const renameKeys = (keysMap, obj) => Object.keys(obj).reduce((acc, key) => ({
            ... acc,
            ... keysMap[key] ? { [keysMap[key] || key]: obj[key] } : {}
        }), {});
        const convertFieldTypes = (o) => {
            Object.keys(o).map(key => {
                if(o[key] === undefined || o[key] === null) {
                    delete o[key];
                    return;
                }
                const fieldType = Schema.fields[key];
                o[key] = Schema.convertType(fieldType, o[key]);
            });
            return o;
        };
        const convertFields = (o) => {
            //loop over Schema converters and apply to the appropriate input field
            Schema.converters.map((c) => { //allow for input to be a function over o
                const val = o[c.input]; //check that this input exists, null, falsy are OK
                if(val === undefined ) { return; }
                o[c.output] = (val * c.scaling) + c.offset;
            });
            return o;
        };
        const prepareTimestamp = (_d) => {
            //   { '﻿Date Logged (AET (+10:00))': '27/03/2019 20:52',
            let ts = null;
            let logged = null;
            let received = null;
        //    _.map(Object.keys(_d), (key) => {
            Object.keys(_d).map((key) => { //continues until a true is returned
                const flag1 = key.includes('Logged');
                const flag2 = key.includes('Received');
                if(!flag1 && !flag2) { return; }
                if(flag1) {
                    logged = _d[key];
                    ts = logged;
                    _d.Logged = logged;
                    ts = moment(ts, 'DD/MM/YYYY HH:mm').toDate();
                }
                if(flag2) {
                    received = _d[key];
                }
            });
            return {ts:ts, logged:logged, received:received};
        }; //End of prepareTime
        const tags = renameKeys( this._pentaTags, d);
        let fields = renameKeys(this._pentaFields, d);
        fields = convertFieldTypes(fields);
        fields = convertFields(fields);
        const point = {
            measurement: this.measurement,
            tags: tags,
            fields: fields
        };
        const timestamp = prepareTimestamp(d);
        if(_.isDate(timestamp.ts)) {
            point.timestamp = timestamp.ts;
            point.fields.Logged = timestamp.logged;
            point.fields.Time_Received = timestamp.received;
        }
        return point;
    } //End of pointFromCSV

} //End of DigitalMatter

module.exports = DigitalMatter;