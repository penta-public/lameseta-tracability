
class Schema_0_0_1 {

    static tagSchema() {
        return {
            status: [
                'Heartbeat\ Status', 
                'External\ Power\ Change',
                'Sensor\ Value\ Elapsed\ Time',
                'Start\ Of\ Trip',
                'End\ Of\ Trip',
                'Distance\ Travelled',
                'Elapsed\ Time'
            ],
            sensorid: ['154197', '154416'],
            project_code: '*'
        }
    } //eo tagSchema

    static fieldSchema() {
        return {
    Altitude_m: "i",
    Analog_2: "f",
    Analog_7: "f",
    Analog_8: "f",
    Battery_Voltage_V: "f",
    Cellular_Signal_Strength: "i",
    Driver_Id_Data: "s",
    Driver_Id_Type: "s",
    Gps_Status_Fix_3D: "b",
    Gps_Status_Fix_OK: "b",
    Heading_Degrees: "f",
    Ignition: "s",
    Input4: "f",
    Input_3: "f",
    Input_5: "f",
    Input_6: "f",
    Input_7: "f",
    Input_8: "f",
    Latitude: "f",
    Longitude: "f",
    Output_1: "f",
    Output_2: "f",
    PDOP_Raw: "i",
    Position_Accuracy_m: "i",
    Relative_Humidity_Percent: "i",
    Speed_Accuracy_KmH: "f",
    Speed_KmH: "i",
    Tamper: "s",
    Temperature_C: "f",
    Temperature_Sensor_C: "f",
    Trip_Type_Code: "s"
}
    } //eo tagSchema

} //eo Schema_0_0_1

module.exports = Schema_0_0_1