const FieldType = require('influx').FieldType;

//define outside of class in order to have a static property
let _measurement = '';
let _fields = {
    Altitude_m: FieldType.FLOAT,
    Analog_2: FieldType.FLOAT,
    Analog_7: FieldType.FLOAT,
    Analog_8: FieldType.FLOAT,
    Battery_Voltage_V: FieldType.FLOAT,
    BigChain_Hash: FieldType.STRING,
    Cellular_Signal_Strength: FieldType.INTEGER,
    Device_Name: FieldType.STRING,
    Driver_Id_Data: FieldType.STRING,
    Driver_Id_Type: FieldType.STRING,
    Gps_Status_Fix_3D: FieldType.BOOLEAN,
    Gps_Status_Fix_OK: FieldType.BOOLEAN,
    Heading_Degrees: FieldType.FLOAT,
    Ignition: FieldType.STRING,
    Input4: FieldType.FLOAT,
    Input_3: FieldType.FLOAT,
    Input_5: FieldType.FLOAT,
    Input_6: FieldType.FLOAT,
    Input_7: FieldType.FLOAT,
    Input_8: FieldType.FLOAT,
    Iota_Hash: FieldType.STRING,
    Latitude: FieldType.FLOAT,
    Logged: FieldType.STRING,
    Longitude: FieldType.FLOAT,
    Output_1: FieldType.FLOAT,
    Output_2: FieldType.FLOAT,
    PDOP_Raw: FieldType.FLOAT,
    Penta_Hash: FieldType.STRING,
    Position_Accuracy_m: FieldType.INTEGER,
    Relative_Humidity_Percent: FieldType.INTEGER,
    Sensor_Id: FieldType.STRING,
    Sensor_Type: FieldType.STRING,
    Speed_Accuracy_KmH: FieldType.FLOAT,
    Speed_KmH: FieldType.FLOAT,
    Status: FieldType.STRING,
    Supply_Chain_State: FieldType.STRING,
    Tag_Value: FieldType.FLOAT,
    Tamper: FieldType.STRING,
    Temperature_C: FieldType.FLOAT,
    Temperature_Sensor_C: FieldType.FLOAT,
    Trip_Type_Code: FieldType.STRING
};
let _tags = [
    'Company', 
    'Project',
    'SensorID',
    'Tag_Data_Hash'
]; 
class Schema_0_0_2 { //use es6 getters/setters: https://coryrylan.com/blog/javascript-es6-class-syntax

    static get measurement() { return _measurement; }
    static set measurement(__measurement) { _measurement = __measurement; }
    static get tags() { return _tags; }
    static get fields() { return _fields; }

} //eo Schema_0_0_2

module.exports = Schema_0_0_2