const FieldType = require('influx').FieldType;

//define outside of class in order to have a static property
let _measurement = '';
let _fields = {
    "Address": FieldType.STRING,
    "Altitude_f": FieldType.FLOAT,
    "Altitude_m": FieldType.FLOAT,
    "Ambient": FieldType.FLOAT,
    "Analog_1": FieldType.FLOAT,
    "Analog_2": FieldType.FLOAT,
    "Analog_3": FieldType.FLOAT,
    "Analog_4": FieldType.FLOAT,
    "Analog_5": FieldType.FLOAT,
    "Analog_6": FieldType.FLOAT,
    "Analog_7": FieldType.FLOAT,
    "Analog_8": FieldType.FLOAT,
    "Analog_9": FieldType.FLOAT,
    "Analog_10": FieldType.FLOAT,
    "Analog_11": FieldType.FLOAT,
    "Analog_12": FieldType.FLOAT,
    "Analog_13": FieldType.FLOAT,
    "Analog_14": FieldType.FLOAT,
    "Analog_15": FieldType.FLOAT,
    "Analog_16": FieldType.FLOAT,
    "Analog_17": FieldType.FLOAT,
    "Analog_18": FieldType.FLOAT,
    "Analog_19": FieldType.FLOAT,
    "Analog_20": FieldType.FLOAT,
    "Battery_Voltage_Percent": FieldType.INTEGER,
    "Battery_Voltage_V": FieldType.FLOAT,
    "BigChain_Hash": FieldType.STRING,
    "Cellular_Signal_Strength": FieldType.INTEGER,
    "Date_Received": FieldType.STRING,
    "Device_Name": FieldType.STRING,
    "Digital_Inputs": FieldType.STRING,
    "Digital_Outputs": FieldType.STRING,
    "Driver_Id_Data": FieldType.STRING,
    "Driver_Id_Type": FieldType.STRING,
    "Gps_Status_Fix_3D": FieldType.BOOLEAN,
    "Gps_Status_Fix_OK": FieldType.BOOLEAN,
    "Heading_Degrees": FieldType.FLOAT,
    "Ignition": FieldType.STRING,
    "Input_1": FieldType.FLOAT,
    "Input_2": FieldType.FLOAT,
    "Input_3": FieldType.FLOAT,
    "Input_4": FieldType.FLOAT,
    "Input_5": FieldType.FLOAT,
    "Input_6": FieldType.FLOAT,
    "Input_7": FieldType.FLOAT,
    "Input_8": FieldType.FLOAT,
    "Iota_Hash": FieldType.STRING,
    "Latitude": FieldType.FLOAT,
    "Logged": FieldType.STRING,
    "Longitude": FieldType.FLOAT,
    "Luminosity": FieldType.FLOAT,
    "Output_1": FieldType.FLOAT,
    "Output_2": FieldType.FLOAT,
    "Output_3": FieldType.FLOAT,
    "Output_4": FieldType.FLOAT,
    "Output_5": FieldType.FLOAT,
    "PDOP_Raw": FieldType.FLOAT,
    "Penta_Hash": FieldType.STRING,
    "Position_Accuracy_f": FieldType.INTEGER,
    "Position_Accuracy_m": FieldType.INTEGER,
    "Pressure": FieldType.INTEGER,
    "Project Code": FieldType.STRING,
    "Relative_Humidity_Percent": FieldType.INTEGER,
    "Resultant": FieldType.FLOAT,
    "SHK": FieldType.INTEGER,
    "Sensor_Id": FieldType.STRING,
    "Sensor_Type": FieldType.STRING,
    "Speed_Accuracy_KmH": FieldType.FLOAT,
    "Speed_KmH": FieldType.FLOAT,
    "Status": FieldType.STRING,
    "Supply_Chain_State": FieldType.STRING,
    "Tag_Value": FieldType.FLOAT,
    "Tamper": FieldType.INTEGER,
    "Temperature_C": FieldType.FLOAT,
    "Temperature_F": FieldType.FLOAT,
    "Temperature_Sensor_C": FieldType.FLOAT,
    "Temperature_Sensor_F": FieldType.FLOAT,
    "Time_Received": FieldType.String,
    "Tilt_Shock": FieldType.FLOAT,
    "Trip_Type_Code": FieldType.STRING,
    "Used_Speed_Limit_MpH": FieldType.INTEGER,
    "X": FieldType.INTEGER,
    "Y": FieldType.INTEGER,
    "Z": FieldType.INTEGER
};
let _tags = [
    'Company', 
    'Project',
    'SensorID',
    'Tag_Data_Hash'
];
let _converters = [ //NOTE: ORDER IS CRITICAL HERE AS CONVERTERS ARE APPLIED IN SEQUENCE AND MAY OVERWRITE ONE ANOTHER
    {sensor:"RoamBee", input:"Temperature_C", output:"Temperature_Sensor_C", type:"Temperature (C)", unit:"C", scaling:1.0, offset:0, decimals:1},
    {sensor:"RoamBee", input:"Temperature_F", output:"Temperature_Sensor_F", type:"Temperature (F)", unit:"F", scaling:1.0, offset:0, decimals:1},    
    {sensor:"Sting", input:"Analog_1", output:"Battery_Voltage_V", type:"General", unit:"V", scaling:0.001, offset:0, decimals:1},
    {sensor:"Sting", input:"Analog_3", output:"Temperature_C", type:"Temperature (C)", unit:"C", scaling:0.01, offset:0, decimals:1},
    {sensor:"Sting", input:"Analog_3", output:"Temperature_F", type:"Temperature (F)", unit:"F", scaling:0.018, offset:32, decimals:1},
    {sensor:"Sting", input:"Analog_4", output:"Cellular_Signal_Strength", type:"Signal Strength GSM", unit:"", scaling:1, offset:0, decimals:0},
    {sensor:"Sting", input:"Analog_5", output:"Temperature_Sensor_C", type:"Temperature (C)", unit:"C", scaling:0.01, offset:0, decimals:2},
    {sensor:"Sting", input:"Analog_5", output:"Temperature_Sensor_F", type:"Temperature (F)", unit:"F", scaling:0.018, offset:32, decimals:2},    
    {sensor:"Sting", input:"Analog_6", output:"Relative_Humidity_Percent", type:"General", unit:"%", scaling:0.01, offset:0, decimals:2},
    {sensor:"Sting", input:"Analog_11", output:"Luminosity", type:"General", unit:"LUX", scaling:0.001, offset:0, decimals:3}
];
class Schema_0_0_4 { //use es6 getters/setters: https://coryrylan.com/blog/javascript-es6-class-syntax

    static get measurement() { return _measurement; }
    static set measurement(__measurement) { _measurement = __measurement; }
    static get tags() { return _tags; }
    static get fields() { return _fields; }
    static convertType(fieldType, d) {
        switch(fieldType) {
            case FieldType.INTEGER:
                d = d.replace(/\s/g,''); //trim all whitespace
                d = parseInt(d, 10);
            break;
            case FieldType.FLOAT:
                d = d.replace(/\s/g,'');
                d = parseFloat(d);
            break;
            case FieldType.BOOLEAN:
                d = d.toLowerCase().trim();
                d = d === 'false' || d === 'f' ? false : d;
                d = new Boolean(d).valueOf();
            break;
            case FieldType.STRING: //can also use ''+d
                d = String(d);
            break;
            default:
                d = String(d);
        } //eo switch
        return d;
    }
    static get converters() { return _converters; }

} //eo Schema_0_0_4

module.exports = Schema_0_0_4