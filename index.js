require('dotenv').config(); //GET THE ENVIRONMENT
const express = require('express');
const _ = require('lodash');
const bodyParser = require('body-parser');
const winston  = require('./winston/winston')(__filename);
const path = require('path');
const csv = require('fast-csv');
const { onUpload, onDeleteFile } = require('./lib/fileuploader');

const    USESTINGER = true; //for debugging purposes, set to true in production
const    stinger = USESTINGER ? require('./clients/stinger') : undefined;

//test app for Sting sensor input
const app = express();
//for uploading csv files
const public = path.join(__dirname, '../public');
app.use('/upload', express.static(public));
// create application/json parser
const jsonParser = bodyParser.json()
// create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const PORT = 8081;

const doLogin = (req, res) => {
    if (!req.body) return res.sendStatus(400)
    res.send('welcome, ' + req.body.username)
};

const doJson = (req, res) => {
    if (!req.body) return res.sendStatus(400)
    // create user in req.body
};

const read154197 = (req, res) => {
    const success = (d) => {
        res.send(JSON.stringify(d));
    }
    stinger.start();
    stinger.influxRead('154197').then(success, () => res.send('Error influxRead'));
};

const testCSV = (req, res) => {
    const success = () => {
        res.send('Test CSV File(s) Write Success');
    }
    stinger.start();
    stinger.writeCSV().then(success, () => res.send('Test CSV File(s) Write Fail'));
}

const testRoamBee = (req, res) => {
    const success = () => {
        res.send('Test CSV File(s) Write Success');
    }
    const d = {file:'./Scaffold/Digital_Matter/data/archive/JackBrown_221226_Report-2.csv', measurement:'220226'};

    stinger.start();
    stinger.writeCSV(d, 'RoamBee').then(success, () => res.send('Test CSV File(s) Write Fail'));
}

const testJSON = (req, res) => {
    const success = () => {
        res.send('Test JSON File(s) Write Success');
    };
    const ISTEST = false; //set as needed
    let json = req.body;
    json = json ? json : {status:'Test JSON File(s) Write Failure'};
    stinger.start();
    //uncomment or comment as apropropriate
    ISTEST ? res.json(json) : stinger.writeJSON(json).then(success, () => res.send('Test JSON File(s) Write Fail'));
}
 
// POST /login gets urlencoded bodies
app.post('/login', urlencodedParser, doLogin)
// POST /api/users gets JSON bodies
app.post('/api/users', jsonParser, doJson);
// GET /api/csv; tested OK
app.get('/api/read154197', read154197);
app.get('/api/testcsv', testCSV);
app.post('/api/testjson', jsonParser, testJSON);
app.post('/uploads', onUpload);
app.get('/api/testRoamBee', testRoamBee);

app.listen(PORT, () => {
    stinger ? stinger.start() : _.noop;
    console.log(`Stinger app listening on port ${PORT}!`)
    winston.info(`Stinger app listening on port ${PORT}!`)
});
