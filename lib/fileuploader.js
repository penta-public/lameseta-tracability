/**
 * NodeJs Server-Side Example for Fine Uploader (traditional endpoints).
 * Maintained by Widen Enterprises.
 *
 * This example:
 *  - handles non-CORS environments
 *  - handles delete file requests assuming the method is DELETE
 *  - Ensures the file size does not exceed the max
 *  - Handles chunked upload requests
 *
 * Requirements:
 *  - express (for handling requests)
 *  - rimraf (for "rm -rf" support)
 *  - multiparty (for parsing request payloads)
 *  - mkdirp (for "mkdir -p" support)
 */

// Dependencies
const    fs = require('fs');
const    rimraf = require('rimraf');
const    mkdirp = require('mkdirp');
const    multiparty = require('multiparty');
const    path = require('path');
const    moment = require('moment');

const    _  = require('lodash');
const    Schema  = require('../data/schema.0.0.4'); //0.0.1 is Corlysis
const    DigitalMatter = require('../data/digitalmatter.js');
const    USESTINGER = true; //for debugging purposes, set to true in production
const    stinger = USESTINGER ? require('../clients/stinger') : undefined;

// paths/constants
const    fileInputName = process.env.FILE_INPUT_NAME || "qqfile";
const    publicDir = process.env.PUBLIC_DIR;
const    nodeModulesDir = process.env.NODE_MODULES_DIR;
const    uploadedFilesPath = path.join(__dirname, '../DeviceData');
const    chunkDirName = "chunks";
const    maxFileSize = process.env.MAX_FILE_SIZE || 0; // in bytes, 0 for unlimited

let config = { //one time init from .env file
    protocol: process.env.INFLUX_PROTOCOL,
    host: process.env.INFLUX_HOST,
    port: process.env.INFLUX_PORT,
    username: process.env.INFLUX_USERNAME,
    password: process.env.INFLUX_PASSWORD,
    database: process.env.INFLUX_DATABASE,
    uploadDir: process.env.DATA_UPLOAD_DIRECTORY
};
config = _.pickBy(config, val => !_.isNil(val)); //use only defined values, skip otherwise
const serverURL      = () => `${config.protocol}://${config.host}:${config.port}`;

/* // routes
app.use(express.static(publicDir));
app.use("/node_modules", express.static(nodeModulesDir));
app.post("/uploads", onUpload);
app.delete("/uploads/:uuid", onDeleteFile); */

const failWithTooBigFile = (responseData, res) => {
    responseData.error = "Too big!";
    responseData.preventRetry = true;
    res.send(responseData);
}; //eo failWithTooBigFile

const isValid = (size) => {
    return maxFileSize === 0 || size < maxFileSize;
}; //eo isValid

const moveFile = (destinationDir, sourceFile, destinationFile, success, failure) => {
    mkdirp(destinationDir, (error) => {
        let sourceStream;
        let destStream;
        if (error) {
            console.error("Problem creating directory " + destinationDir + ": " + error);
            failure();
        } else {
            sourceStream = fs.createReadStream(sourceFile);
            destStream = fs.createWriteStream(destinationFile);
            sourceStream
            .on("error", (error) => {
                console.error("Problem copying file: " + error.stack);
                destStream.end();
                failure();
            })
            .on("end", function(){
                destStream.end();
                success();
            })
            .pipe(destStream);
        } //eo else
    }); //eo  mkdirp
}; //eo moveFile

const onChunkedUpload = (fields, file, res) => {
    const size = parseInt(fields.qqtotalfilesize);
    const uuid = fields.qquuid;
    const index = fields.qqpartindex;
    const totalParts = parseInt(fields.qqtotalparts);
    const responseData = { success: false };
    const failure = () => {
        responseData.error = "Problem storing the chunk!";
        res.send(responseData);
    }; //eo failure
    const storeChunk = (file, uuid, index, numChunks, success, failure) => {
        const getChunkFilename = (index, count) => {
            const digits = new String(count).length;
            const zeros = new Array(digits + 1).join("0");
            return (zeros + index).slice(-digits);
        }; //eo getChunkFilename
        const destinationDir = uploadedFilesPath + uuid + "/" + chunkDirName + "/";
        const chunkFilename = getChunkFilename(index, numChunks);
        const fileDestination = destinationDir + chunkFilename;

        moveFile(destinationDir, file.path, fileDestination, success, failure);
    }; //eo storeChunk
    const combineChunks = (file, uuid, success, failure) => {
        const chunksDir = uploadedFilesPath + uuid + "/" + chunkDirName + "/";
        const destinationDir = uploadedFilesPath + uuid + "/";
        const fileDestination = destinationDir + file.name;
        const appendToStream = (destStream, srcDir, srcFilesnames, index, success, failure) => {
            if (index < srcFilesnames.length) {
                fs.createReadStream(srcDir + srcFilesnames[index])
                .on("end", () => {
                    appendToStream(destStream, srcDir, srcFilesnames, index + 1, success, failure);
                })
                .on("error", (error) => {
                    console.error("Problem appending chunk! " + error);
                    destStream.end();
                    failure();
                })
                .pipe(destStream, {end: false});
            } else {
                destStream.end();
                success();
            }
        }; //eo appendToStream

        fs.readdir(chunksDir, (err, fileNames) => {
            let destFileStream;
            if (err) {
                console.error("Problem listing chunks! " + err);
                failure();
            } else {
                fileNames.sort();
                destFileStream = fs.createWriteStream(fileDestination, {flags: "a"});
                appendToStream(destFileStream, chunksDir, fileNames, 0, function() {
                    rimraf(chunksDir, function(rimrafError) {
                        if (rimrafError) {
                            console.log("Problem deleting chunks dir! " + rimrafError);
                        }
                    });
                    success();
                },
                failure);
            }
        });
    }; //eo combineChunks
    const chunky = () => {
        const success = () => {
            responseData.success = true;
            res.send(responseData);
        };
        const failure = () => {
            responseData.error = "Problem conbining the chunks!";
            res.send(responseData);
        };
        if (index < totalParts - 1) {
            responseData.success = true;
            res.send(responseData);
        } else {
            combineChunks(file, uuid, success, failure);
        }
    }; //eo chunky

    file.name = fields.qqfilename;
    isValid(size) ? storeChunk(file, uuid, index, totalParts, chunky, failure) : failWithTooBigFile(responseData, res);
}; //eo onChunkedUpload

const onSimpleUpload = (fields, file, res) => {
    let filename; //it is an array
    const fileObject = {};
    const uuid = fields.qquuid;
    const responseData = { success: false };
    const moveUploadedFile = (file, uuid, success, failure) => {
        filename = filename.split(/_| /);
        const measurement = filename.length > 1 ? filename[1] : moment().format('MMDDYYYY');
        filename = filename.join('_');
        const destinationDir = uploadedFilesPath + "/";
        const fileDestination = destinationDir + filename;
        fileObject.file = fileDestination;
        fileObject.measurement = measurement;
        moveFile(destinationDir, file.path, fileDestination, success, failure);
    }; //eo moveUploadedFile
    const moveSuccess = () => {
        const success = () => {
            responseData.success = true;
            res.send(responseData);
        };
        fileObject && stinger ? stinger.writeCSV(fileObject).then(success) : success();
    };
    const moveFailure = () => {
        responseData.error = "Problem copying the file!";
        res.send(responseData);
    };

    file.name = fields.qqfilename;
    filename = file.name[0]; //it is an array
    isValid(file.size) ? moveUploadedFile(file, uuid, moveSuccess, moveFailure) : failWithTooBigFile(responseData, res);
};

const writeFilesInflux = async ( uploadPath ) => { 

    const doWrite = async ( fName ) => {
            try { 
                let sName    = /\d{6}/.exec(fName);
                console.log( "writeFilesInflux : sName " + sName );
                let dataPair = { file: fName, measurement : sName[0] }; 
                dataPair.file = path.join(uploadedFilesPath, fName);
                console.log( "writeFilesInflux : datapair " + JSON.stringify(datapair) );
                await stinger.influxWrite( dataPair ); 
            } catch(err) { 
                console.log("Error: " + err);
            };
    };

    fs.readdir( uploadPath, function (err, dataFiles) {
        
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        };
        dataFiles.forEach( function( fName ) {
            doWrite( fName );
        });
    });
    return Promise.resolve();
}; //eo writeFilesInflux

const onUpload = (req, res) => {
    const form = new multiparty.Form();
    form.parse(req, (err, fields, files) => {
        const partIndex = fields.qqpartindex;
        const _files = files[fileInputName][0];
        // text/plain is required to ensure support for IE9 and older
        res.set("Content-Type", "text/plain");
        partIndex == null ? onSimpleUpload(fields, _files, res) : onChunkedUpload(fields, _files, res);
        writeFilesInflux( uploadedFilesPath );
    });
}; //eo onUpload

const onDeleteFile = (req, res) => {
    const uuid = req.params.uuid;
    const dirToDelete = uploadedFilesPath + uuid;
    rimraf(dirToDelete, (error) => {
        if (error) {
            console.error("Problem deleting file! " + error);
            res.status(500);
        }
        res.send();
    });
}; //eo onDeleteFile

module.exports = { onUpload, onDeleteFile }