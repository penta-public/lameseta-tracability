#!/bin/bash
if [ -d "./DeviceData" ];
then
    if [ "$(ls -A ${NewDirectory})" ];
    then
        today=$(date +%d-%m-%Y)
        NewDirectory="./DeviceData-${today}"
         mkdir $NewDirectory
        mv ./DeviceData/* $NewDirectory
    fi
fi
# node ./server.js