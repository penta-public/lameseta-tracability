const crypto = require('crypto');

let Configuration = {
    masterkey        : Buffer.from( process.env.MASTER_KEY, 'utf8'),
    saltLength       : Number( process.env.SALT_LENGTH ),
    ivLength         : Number( process.env.IV_LENGTH   ),
    keyLength        : Number( process.env.KEY_LENGTH  ),
    tagLength        : Number( process.env.TAG_LENGTH  ),
    key_algorithm    : 'sha512',
    master_algorithm : 'aes-256-gcm',
    noTries          : 1523
}

class aes_256_gcm {

    constructor () {
    };

    generateSalt(length) {
        return crypto.randomBytes(Math.ceil(length/2))
                .toString('hex') 
                .slice(0,length); 
    };

    encrypt( plainText ){

        const salt      = crypto.randomBytes(Configuration.saltLength);
        const iv        = crypto.randomBytes(Configuration.ivLength);
        const key       = crypto.pbkdf2Sync(Configuration.masterkey, salt, Configuration.noTries, Configuration.keyLength, Configuration.key_algorithm);
        const cipher    = crypto.createCipheriv(Configuration.master_algorithm, key, iv);
        const encrypted = Buffer.concat([ cipher.update(plainText, 'utf8'), cipher.final() ]);
        const tag       = cipher.getAuthTag();

        return Buffer.concat([salt, iv, tag, encrypted]).toString('base64');
    };

    
    decrypt( cipherText ) {

        // Assume a specifc format for the present.
        // Bytes 0-63 are the salt, 
        // Bytes 64-79 are the IV, 
        // Bytes 90-95 are the tag
        // Bytes starting from 96 are the payload. 

        const base64Data = Buffer.from(cipherText, 'base64');

        const salt       = base64Data.slice(0, 64);
        const iv         = base64Data.slice(64, 80);
        const tag        = base64Data.slice(80, 96);
        const text       = base64Data.slice(96);

        const key        = crypto.pbkdf2Sync(Configuration.masterkey, salt , Configuration.noTries, Configuration.keyLength, Configuration.key_algorithm);
        const decipher   = crypto.createDecipheriv(Configuration.master_algorithm, key, iv);
        decipher.setAuthTag(tag);
        const plainText  = decipher.update(text, 'binary', 'utf8') + decipher.final('utf8');

        return plainText;
    }
};

module.exports.aes_256_gcm = aes_256_gcm;