const crypto = require('crypto');
class Salt {
    constructor(hashAlg, length) {
        this.hash = hashAlg;
        this.salt = this.generateSalt(length);
    }
    ;
    generateSalt(length) {
        return crypto.randomBytes(Math.ceil(length / 2))
            .toString('hex')
            .slice(0, length);
    }
    ;
    saltData(data) {
        let hash = crypto.createHmac(this.hash, this.salt);
        hash.update(data);
        let value = hash.digest('hex');
        return value;
    }
    ;
}
