'use strict';
require('dotenv').config(); 
const crypto = require('crypto');

class Salt {

    constructor( hashAlg, length ) {
        this.hashAlg    = process.env.HASH_ALG;
        this.saltLength = process.env.SALT_LENGTH;
        this.salt       = this.generateSalt( this.saltLength );
    };
    
    generateSalt(length) {
        return crypto.randomBytes(Math.ceil(length/2))
                .toString('hex') 
                .slice(0,length); 
    };

    saltData( data ) {
        let hash = crypto.createHmac(this.hashAlg, this.salt); 
        hash.update(data);
        let value = hash.digest('hex');
        return value;
    };
};
module.exports.Salt = Salt; 