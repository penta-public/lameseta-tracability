const { createLogger, format, transports } = require('winston');
const fs = require('fs');
const path = require('path');
const DailyRotateFile = require('winston-daily-rotate-file');
const env = process.env.NODE_ENV || 'development';
const LOG_DIR = path.normalize(`${process.cwd()}/logs`); //set up logs directory

if (!fs.existsSync(LOG_DIR)) { fs.mkdirSync(LOG_DIR); }
const ConsoleFormatter = format.combine(
    format.colorize(),
    format.printf((info) => {
      const { timestamp, level, message, label, ...args } = info;
      return `${timestamp} ${level} [${info.label}]: ${message} \n${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
    }) //eo printf
  ); //eo format
  const FileFormatter = format.combine(
    format.printf((info) => {
      const { timestamp, level, message, label, ...args } = info;
      return `${timestamp} ${level} [${info.label}]: ${message} \n${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
    }) //eo printf
  ); //eo format
const logger = caller => {
  
    return createLogger({
      // change level if in dev environment versus production
      level: 'info', //env === 'production' ? 'info' : 'debug',
      format: format.combine(
        format.label({ label: path.basename(caller) }),
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.prettyPrint()
      ),
      transports: [
        new transports.Console({ format: ConsoleFormatter }), //eo console
        new DailyRotateFile({
            dirname: LOG_DIR,
            filename: '%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: false,
            maxSize: '1m',
            maxFiles: '60d',
            level: 'debug'
            //format: FileFormatter
          }) //eo DailyRotateFile
      ]
    }); //eo createLogger

  }; //eo logger

  //for err.stack output
logger.stack = (str) => str.replace(/\n/g, '').replace(/    at/g, ' @')
  
module.exports = logger;